- [ ] lazy grid instantiation
  - lazy cell instantiation
  - create cell on demand

- key-access BinaryHeap with recalculation/sinking
- A-star
- A-star with simplify
  - start at cell, look ahead until movement illegal
  - add waypoint and start over until end reached
- theta-star