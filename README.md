Move a group of tokens at once towards a target.

This module is intended for scenes with lots and lots of tokens where manual movement would be too tedious.

# Installation

# Features

# Limitations
Movement has lots of variables involved. Creature size, wall arrangement, environmental effects like spells or terrain
features... they can't all be taken into account. Moving large masses of tokens, simplicity and some degree
of manual adjustment is considered better than overly complex logic.

- Generally aimed at medium-sized tokens. Larger or smaller tokens won't move quite as well
- spell effects ignored (difficult terrain)
  - though with coding effort that could be addressed, other modules do that
- can be slow on large maps!
  - inefficient implementation
- 


# Future Ideas
- see [ideas.md]

# Contributions