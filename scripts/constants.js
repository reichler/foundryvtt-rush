const CONSTANTS = {
    MODULE_NAME: "rush",
    SEARCH_MODE: 'A*',
    GRID_DIRECTIONS: [-1, 0, 1],
    LABEL_LOC_STYLE: new PIXI.TextStyle({fontFamily: 'Arial', fontSize: 10, fontWeight: 'lighter'})
}

export default CONSTANTS;